ymaps.ready(init);
function init(){
    let pointersCollection = new ymaps.GeoObjectCollection();

    function generateBalloonContent(address, id) {
        const balloonContent = `<div class="balloon"><div class="balloon__text">Пункт по адресу ${address}</div> `+
        `<a class=\"balloon__btn btn\" href=\"/catalog/${id}\">Выбрать велосипед</a></div>`
        return balloonContent;
    }

    let myMap = new ymaps.Map("map", {
        center: [55.02, 82.91],
        zoom: 12
    });

    api.getPointers().then((pointersInfo) => {
        pointersInfo.forEach(pointer => {
            const balloonContent = generateBalloonContent(pointer.address, pointer._id);
            pointersCollection.add(
                new ymaps.Placemark(pointer.coordinates, {
                    balloonContentBody: balloonContent
                }));
        });
    })

    myMap.geoObjects.add(pointersCollection);
}