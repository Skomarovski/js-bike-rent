const inputsList = document.querySelectorAll('.input');
const cardNumber = document.querySelector('.number');
const validityPeriod = document.querySelector('.validity-period');
const cvvCode = document.querySelector('.cvv');
const cardButton = document.querySelector('.card-requisites__btn');
const form = document.querySelector('.form');

cardNumber.addEventListener('blur', () => cardNumberValidate(cardNumber));
validityPeriod.addEventListener('blur', () => cardDateValidation(validityPeriod));
cvvCode.addEventListener('blur', () => cvvValidate(cvvCode));
form.addEventListener("submit", (e) => e.preventDefault());

// Валидация номера карты
function cardNumberValidate(cardNumberElem) {
  const value = cardNumberElem.value.replaceAll(' ', '');

  hideError(cardNumberElem);

  if (value !== '') {
    let formatingValue = '';
    if (value.length === 16) {
      for (let i = 0; i < value.length; i++) {
        if (!isNumber(value[i])) {
          renderError('cardNumberInt', cardNumberElem);
          break;
        }
        formatingValue += value[i];
        if ((i+1) % 4 === 0 & i !== 15) {
          formatingValue += ' ';
        }
  
        cardNumberElem.value = formatingValue;
      }
    } else {
      renderError('cardNumberLength', cardNumberElem);
    }
  }
}


// Валидация даты выпуска карты
function cardDateValidation(cardDateElem) {
  const value = cardDateElem.value;

  hideError(cardDateElem);

  if (value !== '') {
    let onlyNumbers = ''
    for (let char of value) {
      if (isNumber(char)) {
        onlyNumbers += char;
      }
    }

    if (onlyNumbers.length === 4) {
      const month = onlyNumbers.slice(0, 2);
      const year = onlyNumbers.slice(2);

      if (!isMonth(month)) {
        renderError('cardDateMonth', cardDateElem);
        return;
      }

      if (!isValidDate(month, year)) {
        renderError('cardDateYear', cardDateElem);
        return;
      }

      cardDateElem.value = `${month}/${year}`;
    } else {
      renderError('cardDateInt', cardDateElem);
    }
  }

  function isMonth(number) {
    if (number[0] === 0) number = number[1];
    const months = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
    return months.includes(Number(number));
  }

  function isValidDate(month, year) {
    const monthNow = (new Date()).getMonth()+1;
    const yearNow = (new Date()).getFullYear().toString().slice(2);
    if (year === yearNow) {
      return month >= monthNow;
    } else {
      return year - 5 < yearNow & year > yearNow;
    }
  }
}


// Валидации CVV кода
function cvvValidate(cardCvvElem) {
  const value = cardCvvElem.value;

  hideError(cardCvvElem);

  if (value !== '') {
    if (value.length !== 3) {
      renderError('cvvLength', cardCvvElem)
    } else {
      for (let char of value) {
        if (!isNumber(char)) {
          renderError('cvvInt', cardCvvElem);
          break;
        }
      }
    }
  }
}


// Рендеринг боксов с текстом ошибки
function renderError(error, elem) {
  errorDict = {
    'cardNumberInt': 'Номер карты должен состоять только из цифр',
    'cardNumberLength': 'Номер карты должен состоять из 16 цифр',
    'cvvInt': 'CVV код должен состоять только из цифр',
    'cvvLength': 'CVV код должен состоять из 3 цифр',
    'cardDateInt': 'Дата должна содержать 4 цифры',
    'cardDateMonth': 'В году 12 месяцев',
    'cardDateYear': 'Некорректная дата'
  };

  const parent = elem.closest('.input-container');
  const tooltip = document.createElement('div');
  tooltip.classList.add('tooltip');
  tooltip.textContent = errorDict[error];
  parent.appendChild(tooltip);

  elem.classList.add('input_error');
}

// Скрытие боксов с ошибками
function hideError(elem) {
  const tooltip = elem.nextSibling;
  if (tooltip) {
    tooltip.remove();
  }
  elem.classList.remove('input_error');
}

// Является ли символ цифрой
function isNumber(char) {
  return !isNaN(Number(char));
}