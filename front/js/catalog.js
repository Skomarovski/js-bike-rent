'use strict';

function init() {
  let page = 1;
  const loadMoreBtn = document.querySelector('#loadMore button');
  loadCatalog(page);
  loadMoreBikes();
  
  function loadMoreBikes() {
    loadMoreBtn.onclick = async () => {
      disableButtonLoadMore();
      await loadCatalog(++page);
      enableButtonLoadMore();
      console.log(page);
    }
  }
}

// Загрузка части каталога
function loadCatalog(page) {
  const id = getPointId();
  api.getBikes(id, page).then((bikesData) => {
    appendCatalog(bikesData.bikesList);
    showButtonLoadMore(bikesData.hasMore);
  })
}

// Рендеринг карточек велосипедов
function appendCatalog(items) {
  const container = document.getElementById('bikeList');
  items.forEach(bikeInfo => {
    const linkToBikePage = `./${bikeInfo._id}`;

    // bikeCard - контейнер для всех элементов карточки
    // велосипеда (bike-card__main, bike-card__footer)
    const bikeCard = document.createElement('div');
    bikeCard.classList.add('bike-card');

    // bikeCardMain - контейнер для верхней части карточки
    const bikeCardMain = document.createElement('div');
    bikeCardMain.classList.add('bike-card__main');
    
    // bikeCardFooter - контенер для нижней части карточки
    const bikeCardFooter = document.createElement('div');
    bikeCardFooter.classList.add('bike-card__footer');

    // bikeImg - изображение велосипеда
    const bikeImg = document.createElement('a');
    bikeImg.classList.add('bike-card__img');
    bikeImg.style.backgroundImage = `url('../images/catalog/${bikeInfo.img}')`;
    bikeImg.href = linkToBikePage;

    // bikeTitle - название велосипеда
    const bikeTitle = document.createElement('a');
    bikeTitle.classList.add('bike-card__title');
    bikeTitle.textContent = bikeInfo.name;
    bikeTitle.href = linkToBikePage;

    // bikeCost - стоимость аренды велосипеда
    const bikeCost = document.createElement('div');
    bikeCost.classList.add('bike-card__cost');
    bikeCost.textContent = `Стоимость за час - ${bikeInfo.cost} ₽`;
    
    // Помещение элементов в блок bikeCardMain
    bikeCardMain.append(bikeImg, bikeTitle);

    // bikeBtn - кнопка для входа на страницу товара
    const bikeBtn = document.createElement('a');
    bikeBtn.classList.add('bike-card__btn', 'btn');
    bikeBtn.textContent = 'Арендовать';
    bikeBtn.href = linkToBikePage;

    // Помещение элементов в блок bikeCardFooter
    bikeCardFooter.append(bikeCost, bikeBtn);

    // Помещение элементов карточки в блок bikeCard
    bikeCard.append(bikeCardMain, bikeCardFooter);

    // Помещение bikeCard в bikeList
    container.appendChild(bikeCard);
  });
}

function showButtonLoadMore(hasMore) {
  const btnLoadMore = document.getElementById('loadMore');
  if (hasMore) {
    btnLoadMore.classList.remove('hidden');
  } else {
    btnLoadMore.classList.add('hidden');
  }
}

function disableButtonLoadMore() {
  const btnLoadMore = document.querySelector('#loadMore button');
  btnLoadMore.disabled = true;
}

function enableButtonLoadMore() {
  const btnLoadMore = document.querySelector('#loadMore button');
  btnLoadMore.disabled = false;
}

// Извлечение Id точки проката из URL
function getPointId() {
  const splitedPageUrl = document.URL.split('/')
  const id = splitedPageUrl[splitedPageUrl.length-1] === 'catalog' 
            ? '' : splitedPageUrl[splitedPageUrl.length-1];
  return id;
}

document.addEventListener('DOMContentLoaded', init)